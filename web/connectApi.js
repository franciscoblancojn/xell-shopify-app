export const connectApi = async ({ data, url }) => {
  try {
    var myHeaders = new Headers();
    myHeaders.append("api-key", process.env.SHOPIFY_API_KEY);
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify(data);
    
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    const respond = await fetch(
      `https://shopi.xell.shop/api-app/v1${url}`,
      requestOptions
    );
    const result = await respond.json();
    if (result.type === "error") {
      throw result;
    }
    return result;
  } catch (error) {
    if (error.type == "error") {
      return error;
    }
    return {
      type: "error",
      error,
    };
  }
};
