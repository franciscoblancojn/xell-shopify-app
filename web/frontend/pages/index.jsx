import { Page, Layout, Spinner } from "@shopify/polaris";
import { getSessionToken, authenticatedFetch } from "@shopify/app-bridge-utils";
import { useAppBridge } from "@shopify/app-bridge-react";
import { logo } from "../assets";

import { useEffect, useState, useReducer, useMemo } from "react";
import axios from "axios";

import env from "./_env"


const request = async (config) => {
  config.url = "https://shopi.xell.shop/api-app/v1" + config.url;
  config.headers = {
    ...(config.headers ?? {}),
    "Content-Type": "application/json",
    "api-key": env.SHOPIFY_API_SECRET,
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return error.response.data;
  }
};
const isConnect = async (shop) => {
  const result = await request({
    method: "get",
    url: `/shop/is-connect?shop=${shop}`,
  });
  return result;
};
const onDisconnectShop = async (shop) => {
  const result = await request({
    method: "delete",
    url: `/shop/disconnect?shop=${shop}`,
  });
  return result;
};
const onGenerateUrlConnect = async (shop) => {
  console.log(shop);
  const shopName = shop.split(".myshopify")[0];
  const result = await request({
    method: "get",
    url: `/shop/generate-url-connect?shopName=${shopName}`,
  });
  return result;
};

const HomePage = () => {
  const [shop, setShop] = useState("");
  const [active, setActive] = useState(false);
  const [load, setLoad] = useState(false);

  const onConnect = async () => {
    // const result = await onGenerateUrlConnect(shop);
    // if (result.type == "ok") {
    //   window.open(result.url);
    //   window.close();
    // } else {
    //   alert(result.msj);
    // }
  };

  const onDisconnect = async () => {
    setLoad(false);
    await onDisconnectShop(shop);
    await onLoad();
  };

  const onLoad = async () => {
    const search = window.location.search;
    const shop = search.split("shop=")[1].split("&")[0];
    setShop(shop);
    const result = await isConnect(shop);
    setActive(result?.connect === true);
    setLoad(true);
  };
  const btnConnect = useMemo(() => {
    if (!active) {
      return (
        <>
          <button className="btn btn-connect" onClick={onConnect}>
            Connect Xell
          </button>
        </>
      );
    }
    return (
      <>
        <p className="text text-connect">Xell is Connect</p>
        <button className="btn btn-disconnect" onClick={onDisconnect}>
          Disconnect Xell
        </button>
      </>
    );
  }, [active]);

  const content = useMemo(() => {
    if (!load) {
      return (
        <>
          <Spinner accessibilityLabel="Spinner example" size="large" />
        </>
      );
    }
    return (
      <>
        <img src={logo} alt="" />
        {btnConnect}
      </>
    );
  }, [load, btnConnect]);

  useEffect(() => {
    onLoad();
  }, []);
  return (
    <>
      <Page narrowWidth>
        <Layout>
          <Layout.Section>
            <div className="content">{content}</div>
          </Layout.Section>
        </Layout>
      </Page>
      <style jsx>{`
        .content {
          display: flex;
          flex-direction: column;
          align-content: center;
          justify-content: center;
          align-items: center;
          gap: 50px;
          min-height: 80vh;
        }
        .text {
          font-size: 20px;
          font-weight: 700;
        }
        .text-connect {
          color: #22cc8c;
        }
        .btn {
          border: 0;
          padding: 20px 60px;
          border-radius: 35px;
          cursor: pointer;
          color: white;
          font-size: 20px;
          font-weight: 700;
        }
        .btn-connect {
          background-image: linear-gradient(to left, #2f1071, #22cc8c);
          color: white;
        }
        .btn-disconnect {
          background-image: linear-gradient(to left, #fc3838, #cc2222);
          color: rgb(235, 235, 235);
        }
      `}</style>
    </>
  );
};
export default HomePage;
