require("module-alias/register");
const db = require("@api/db");
const disconnectHooks = require("@api/controllers/hook/disconnect");

const disconnect = async (req, res) => {
  try {
    const { shop } = req.query;
    if (!shop) {
      throw "shop is required";
    }
    const result = await db.get({
      table: "shop",
      query: {
        shop,
      },
    });
    if (result.length > 0 && result[0].access_token) {
      await disconnectHooks({ shop, access_token: result[0].access_token });
    }

    await db.delete({
      table: "shop",
      where: {
        shop,
      },
    });
    res.status(200).send({
      type: "ok",
      connect: false,
    });
  } catch (error) {
    return res.status(400).send({
      type: "error",
      msj: `${error}`,
      error,
    });
  }
};
module.exports = disconnect;
