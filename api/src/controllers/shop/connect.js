require("module-alias/register");
const db = require("@api/db");
const hook = require("@api/controllers/hook/_index");
const env = require("@api/env");
const shopifyToken = require("@api/shopifyToken");

const connect = async (req, res) => {
  try {
    const { code, shop } = req.query;
    const access = await shopifyToken.getAccessToken(shop, code);
    const { access_token } = access;
    if (!access_token) {
      throw "error access_token";
    }
    await db.put({
      table: "shop",
      where: {
        shop,
      },
      data: {
        $set: {
          shop,
          access_token
        },
      },
      options: {
        upsert: true,
      },
    });
    await hook.connect({ shop, access_token })
    res.status(200).redirect(`https://${shop}/admin/apps/${env.SHOPIFY_API_KEY}/`);
  } catch (error) {
    return res.status(400).send({
      type: "error",
      msj: `${error}`,
      error,
    });
  }
};
module.exports = connect;
