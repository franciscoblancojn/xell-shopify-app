require("module-alias/register");
const db = require("@api/db");

const isConnect = async (req, res) => {
  try {
    const { shop } = req.query;
    const result = await db.get({
      table: "shop",
      query: {
        shop,
      },
    });

    res.status(200).send({
      type: "ok",
      connect: result.length > 0 && result[0].access_token != null,
    });
  } catch (error) {
    return res.status(400).send({
      type: "error",
      msj: `${error}`,
      error,
    });
  }
};
module.exports = isConnect;
