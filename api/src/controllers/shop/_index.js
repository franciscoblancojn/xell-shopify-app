require("module-alias/register");

module.exports = {
  generateUrlConnect: require("@api/controllers/shop/generate-url-connect"),
  connect: require("@api/controllers/shop/connect"),
  isConnect: require("@api/controllers/shop/is-connect"),
  disconnect: require("@api/controllers/shop/disconnect"),
};
