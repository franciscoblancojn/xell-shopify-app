require("module-alias/register");
const shopifyToken = require("@api/shopifyToken");
const db = require("@api/db");

const generateUrlConnect = async (req, res) => {
  try {
    const { shopName, shop, user_id } = req.query;
    if (!shopName) {
      throw "shopName requeride";
    }
    if (shop) {
      const result = await db.get({
        table: "shop",
        query: {
          shop,
        },
      });
      if (result.length > 0) {
        if (result[0].access_token) {
          throw "La Tienda ya esta Connectada";
        }
      }
      await db.put({
        table: "shop",
        where: {
          shop,
        },
        data: {
          $set: {
            shop,
            user_id,
          },
        },
        options: {
          upsert: true,
        },
      });
    }

    const url = shopifyToken.generateAuthUrl(shopName);
    res.status(200).send({
      type: "ok",
      url,
    });
  } catch (error) {
    return res.status(400).send({
      type: "error",
      msj: `${error}`,
      error,
    });
  }
};
module.exports = generateUrlConnect;
