const axios = require("axios");

const getHooks = async ({ shop, access_token }) => {
  try {
    var config = {
      method: "get",
      url: `https://${shop}/admin/api/2022-01/webhooks.json`,
      headers: {
        "X-Shopify-Access-Token": access_token,
        'Content-Type': 'application/json'
      },
    };
    const response = await axios(config);
    return response.data.webhooks;
  } catch (error) {
    return [];
  }
};
module.exports = getHooks;
