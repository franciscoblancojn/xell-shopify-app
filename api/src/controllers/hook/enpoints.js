require("module-alias/register");
const db = require("@api/db");
const disconnectHooks = require("./disconnect");

const enpoints = async (req, res) => {
  try {
    const { type, shop } = req.query;
    const data = req.body;

    await db.post({
      table: `hooks`,
      data: {
        type,
        shop,
        data,
      },
    });

    if ("app/uninstalled" === type) {
      const result = await db.get({
        table: "shop",
        query: {
          shop,
        },
      });
      if (result.length > 0 && result[0].access_token) {
        await disconnectHooks({ shop, access_token: result[0].access_token });
      }
      await db.delete({
        table: "shop",
        where: {
          shop,
        },
      });
    }

    res.status(200).send({
      type: "ok",
    });
  } catch (error) {
    return res.status(400).send({
      type: "error",
      msj: `${error}`,
      error,
    });
  }
};
module.exports = enpoints;
