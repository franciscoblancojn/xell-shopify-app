require("module-alias/register");
const axios = require("axios");
const env = require("@api/env");
const log = require("@api/log");
const getHooks = require("./get");

const disconnectHooks = async ({ shop, access_token }) => {
  var HOOKS = await getHooks({ shop, access_token });

  HOOKS = HOOKS.filter((h) => h.address.indexOf(env.SHOPIFY_API_HOOK) > -1);
  for (let i = 0; i < HOOKS.length; i++) {
    try {
      const HOOK = HOOKS[i];
      var config = {
        method: "delete",
        url: `https://${shop}/admin/api/2022-01/webhooks/${HOOK.id}.json`,
        headers: {
          "X-Shopify-Access-Token": access_token,
          'Content-Type': 'application/json'
        },
      };
      const response = await axios(config);
    } catch (error) {}
  }
};
module.exports = disconnectHooks;
