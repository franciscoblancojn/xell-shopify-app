require("module-alias/register");
const axios = require("axios");
const env = require("@api/env");
const log = require("@api/log");
const disconnectHooks = require("./disconnect");



const HOOKS = [
  "app/uninstalled",

  "customers/create",
  "customers/disable",
  "customers/enable",
  "customers/update",

  "orders/cancelled",
  "orders/create",
  "orders/fulfilled",
  "orders/paid",
  "orders/partially_fulfilled",
  "orders/updated",
  "orders/edited",

  "products/create",
  "products/update",
];

const connectHooks = async ({ shop, access_token }) => {
  await disconnectHooks({ shop, access_token });
  for (let i = 0; i < HOOKS.length; i++) {
    const type = HOOKS[i];
    try {
      const data = JSON.stringify({
        webhook: {
          topic: type,
          address: `${env.SHOPIFY_API_HOOK}/hook?shop=${shop}&type=${type}`,
          format: "json",
        },
      });

      const config = {
        method: "post",
        url: `https://${shop}/admin/api/2022-01/webhooks.json`,
        headers: {
          "X-Shopify-Access-Token": access_token,
          "Content-Type": "application/json",
        },
        data: data,
      };
      const response = await axios(config);
    } catch (error) {}
  }
};
module.exports = connectHooks;
