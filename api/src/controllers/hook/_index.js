require("module-alias/register");

module.exports = {
  enpoints: require("@api/controllers/hook/enpoints"),
  connect: require("@api/controllers/hook/connect"),
};
