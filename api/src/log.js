require("module-alias/register");
const db = require("@api/db");

const log = async (data) => {
  await db.post({
    table: "logs",
    data,
  });
};
module.exports = log;
