require("module-alias/register");

const env = require("@api/env");
const { base64encode } = require("nodejs-base64");

const apiKey = (req, res, next) => {
  const key = req.headers["api-key"];
  if(!key){
    return res.status(400).send({
        type: "error",
        msj: `api-key invalid`,
    });
  }
  if (base64encode(`${key}`) === base64encode(`${env.SHOPIFY_API_SECRET}`)) {
    next();
    return;
  }
  return res.status(400).send({
    type: "error",
    msj: `api-key invalid`,
  });
};

module.exports = apiKey;
