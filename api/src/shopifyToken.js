
const env = require("@api/env");
const ShopifyToken = require("shopify-token");

const SCOPES =
  "read_customers,write_customers,read_products,write_products,read_orders,write_orders";

const shopifyToken = new ShopifyToken({
  sharedSecret: env.SHOPIFY_API_SECRET,
  redirectUri: env.SHOPIFY_API_CONNECT,
  apiKey: env.SHOPIFY_API_KEY,
  scopes: SCOPES,
});

module.exports = shopifyToken;
