require("module-alias/register");

const apiKey = require("@api/middlewares/api-key");
const shop = require("@api/controllers/shop/_index");

const router = require("express").Router();

router.get("/generate-url-connect", [], shop.generateUrlConnect);
router.get("/connect", [], shop.connect);
router.get("/is-connect", [apiKey], shop.isConnect);
router.delete("/disconnect", [apiKey], shop.disconnect);

module.exports = router;
