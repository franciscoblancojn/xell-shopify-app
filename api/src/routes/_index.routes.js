require('module-alias/register')

const router = require('express').Router()

router.use('/shop', require('@api/routes/shop.routes'))
router.use('/hook', require('@api/routes/hook.routes'))

module.exports = router
