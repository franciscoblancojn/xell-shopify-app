require("module-alias/register");

const hook = require("@api/controllers/hook/_index");

const router = require("express").Router();

router.post("/", [], hook.enpoints);

module.exports = router;
